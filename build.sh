#!/bin/zsh

# Copy this file and the Dockerfile to the directory containing TRiP98
# Docker does not allow to copy from absolute nor ../ paths

REPO="andreasga"
NAME="pytrip"

CONTAINER="python-3.6"

# Must be relative path, which is daugther to the run dir
TRIP98PROG=TRiP98/base_tpdata2/base/TRiP98BEAM/
TRIP98BINPATH=TRiP98/bin/

echo "TRIP98PROG: $TRIP98PROG"
echo "TRIP98BINPATH: $TRIP98BINPATH"

sudo docker build --tag ${REPO}/${NAME}:${CONTAINER} \
    --build-arg TRIP98PROG=$TRIP98PROG \
    --build-arg TRIP98BINPATH=$TRIP98BINPATH \
    ./

sudo docker push ${REPO}/${NAME}:${CONTAINER}

# docker run -it --rm ${REPO}/${NAME}:${CONTAINER} bash
