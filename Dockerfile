FROM python:3.6.13-stretch

MAINTAINER Andreas G. Andersen <agravgaard@protonmail.com>

USER root

ENV TRIP98PROG=/opt/TRIP98PROG

RUN mkdir -p $TRIP98PROG

# COPY --from=andreasga/pytrip:python-3.6 $TRIP98PROG/ $TRIP98PROG/
FROM registry.gitlab.com/dcpt-research/pytrip-docker:pytrip as prev_img
COPY --from=prev_img $TRIP98PROG/ $TRIP98PROG/

ENV PATH=$TRIP98PROG:$PATH

# We need i686 binary compatibility, then test
RUN dpkg --add-architecture i386 \
  && apt-get update \
  && apt-get install -y libc6-i386 \
  && TRiP98 -c "QUIT"

RUN pip install pytrip98


