## Build scripts for PyTRiP Docker Image

# Prerequisites:
* Docker (installed by root)
* sudo

# How to build:

## Option 1:
* Create a pull-request or fork of this repository with your changes
* The CI should then trigger a build automatically

## Option 2:
* Install and setup docker
* Run `sudo docker build -t <your-tag-name> .`

# How to use:
* Install and setup docker
* Run the docker image with `sudo docker run -it --rm registry.gitlab.com/dcpt-research/pytrip-docker:pytrip bash`
* You should now be logged in as root in the image
* Report any issues
